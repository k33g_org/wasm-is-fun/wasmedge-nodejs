// 🚧 WIP
const http = require('http')
const url = require('url')
const port = process.env.PORT || 3000
const { say } = require('../pkg/wasmedge_nodejs_starter_lib.js');


const requestHandler = (request, response) => {
  const queryObject = url.parse(request.url,true).query
  response.end(say(queryObject['name'])+ '\n')
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('😡 something bad happened', err)
  }
  console.log(`🌍 serving on ${port}`)
})